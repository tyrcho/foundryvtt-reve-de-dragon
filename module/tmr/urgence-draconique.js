import { ChatUtility } from "../chat-utility.js";
import { Grammar } from "../grammar.js";
import { Misc } from "../misc.js";
import { RdDRollTables } from "../rdd-rolltables.js";
import { tmrColors, tmrConstants, tmrTokenZIndex, TMRUtility } from "../tmr-utility.js";
import { Draconique } from "./draconique.js";

export class UrgenceDraconique extends Draconique {

  constructor() {
    super();
  }

  type() { return 'queue' }
  match(item) { return Draconique.isQueueDragon(item) && Grammar.toLowerCaseNoAccent(item.name).includes('urgence draconique'); }
  manualMessage() { return false }
  async onActorCreateOwned(actor, queue) {
    let coordSortsReserve = (actor.data.data.reve.reserve?.list.map(it => it.coord)) ?? [];
    if (coordSortsReserve.length == 0) {
      // La queue se transforme en idée fixe
      let ideeFixe = await RdDRollTables.getIdeeFixe();
      ChatMessage.create({
        whisper: ChatUtility.getWhisperRecipientsAndGMs(game.user.name),
        content: `En l'absence de sorts en réserve, l'urgence draconique de ${actor.name} se transforme en ${queue.name}`
      });
      await actor.createOwnedItem(ideeFixe);
      await actor.deleteOwnedItem(queue._id);
      return;
    }
    else {
      let demiReve = actor.getDemiReve();
      coordSortsReserve.sort((a, b) => TMRUtility.distanceTMR(a, demiReve) - TMRUtility.distanceTMR(b, demiReve));
      let tmr = TMRUtility.getTMR(coordSortsReserve[0]);
      await this.createCaseTmr(actor, 'Urgence draconique: ' + tmr.label, tmr, queue._id);
    }
  }

  async onActorDeleteCaseTmr(actor, casetmr) {
    await actor.deleteOwnedItem(casetmr.data.sourceid);
  }

  code() { return 'urgence' }
  tooltip(linkData) { return `Urgence draconique!` }
  img() { return 'icons/svg/hazard.svg' }

  createSprite(pixiTMR) {
    return pixiTMR.sprite(this.code(),
      {
        zIndex: tmrTokenZIndex.conquete,
        color: tmrColors.queues,
        taille: tmrConstants.full,
        decallage: { x: 2, y: 0 }
      });
  }
}
