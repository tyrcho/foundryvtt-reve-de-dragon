import { Grammar } from "../grammar.js";
import { tmrColors, tmrConstants, tmrTokenZIndex, TMRUtility } from "../tmr-utility.js";
import { Draconique } from "./draconique.js";

export class Periple extends Draconique {

  constructor() {
    super();
  }

  type() { return 'souffle' }
  match(item) { return Draconique.isSouffleDragon(item) && Grammar.toLowerCaseNoAccent(item.name).includes('periple'); }
  manualMessage() { return false }

  async onActorCreateOwned(actor, souffle) {
    let terrain = new Roll("1d2").evaluate().total == 1 ? 'sanctuaire' : 'necropole';
    let tmrs = TMRUtility.getListTMR(terrain);
    for (let tmr of tmrs) {
      await this.createCaseTmr(actor, 'Périple: ' + tmr.label, tmr, souffle._id);
    }
  }


  code() { return 'periple' }
  tooltip(linkData) { return `Votre Périple passe par ${this.tmrLabel(linkData)}` }
  img() { return 'icons/svg/acid.svg' }

  createSprite(pixiTMR) {
    return pixiTMR.sprite(this.code(), {
      zIndex: tmrTokenZIndex.conquete,
      alpha: 1,
      color: tmrColors.souffle,
      taille: tmrConstants.twoThird,
      decallage: tmrConstants.right
    });
  }
  getDifficulte(tmr) {
    switch (tmr.type) {
      case 'sanctuaire': return -3;
      case 'necropole': return -5;
    }
    return 0;
  }
}