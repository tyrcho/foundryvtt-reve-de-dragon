import { Misc } from "./misc.js";

/* -------------------------------------------- */
/**
 * Mapping des types d'Item/Actor vers le nom d'affichage.
 * Par défaut, on prend le type avec la première lettre
 * majuscule, pas besoin d'ajouter tous les types.
 */
const typeDisplayName = {
  "competence": "Compétence",
  "herbe": "Plante",
  "ingredient": "Ingrédient",
  "queue": "Queue de dragon",
  "ombre": "Ombre de Thanatos",
  "souffle": "Souffle de Dragon",
  "tete": "Tête de Dragon",
  "ingredient": "Ingrédient",
  "rencontresTMR": "Rencontre des TMR",
  "competencecreature": "Compétence de créature",
  "nombreastral": "Nombre astral",
  "casetmr": "Effet sur TMR",
  "recettealchimique": "Recette alchimique",
  "recettecuisine": "Recette de cuisine",
  "tarot": "Carte de tarot draconique",
  "tache": "Tâche",
  "meditation": "Méditation",
  "musique": "Morceau de musique",
  "chant": "Chanson",
  "creature": "Créature",
  "entite": "Entité",
  "vehicule": "Véhicule"
}

export class RddCompendiumOrganiser {
  static init() {
    Hooks.on('renderCompendium', async (pack, html, data) => RddCompendiumOrganiser.onRenderCompendium(pack, html, data))
  }

  static async onRenderCompendium(pack, html, data) {
    console.log('onRenderCompendium', pack, html, data);
    if (pack.metadata.system === 'foundryvtt-reve-de-dragon') {
      const content = await pack.getContent();

      html.find('.directory-item').each((i, element) => {
        let entity = content.find(it => it._id === element.dataset.entryId);

        if (entity?.entity === 'Actor' || entity?.entity === 'Item') {
          const typeName = typeDisplayName[entity.data.type] ?? Misc.upperFirst(entity.data.type);
          RddCompendiumOrganiser.insertEntityType(element, typeName);
        }
      });
    }
  }

  static insertEntityType(element, type) {
    element.children[1].insertAdjacentHTML('afterbegin', `<label>${type}: </label>`);
  }

}