
const monnaiesData = [
  {
    _id: randomID(16), name: "Etain (1 denier)", type: 'monnaie',
    img: "systems/foundryvtt-reve-de-dragon/icons/objets/piece_etain_poisson.webp",
    data: { quantite: 0, valeur_deniers: 1, encombrement: 0.001, description: "" }
  },
  {
    _id: randomID(16), name: "Bronze (10 deniers)", type: 'monnaie',
    img: "systems/foundryvtt-reve-de-dragon/icons/objets/piece_bronze_epees.webp",
    data: { quantite: 0, valeur_deniers: 10, encombrement: 0.002, description: "" }
  },
  {
    _id: randomID(16), name: "Argent (1 sol)", type: 'monnaie',
    img: "systems/foundryvtt-reve-de-dragon/icons/objets/piece_argent_sol.webp",
    data: { quantite: 0, valeur_deniers: 100, encombrement: 0.003, description: "" }
  },
  {
    _id: randomID(16), name: "Or (10 sols)", type: 'monnaie',
    img: "systems/foundryvtt-reve-de-dragon/icons/objets/piece_or_sol.webp",
    data: { quantite: 0, valeur_deniers: 1000, encombrement: 0.004, description: "" }
  }
]

export class Monnaie {

  static monnaiesData() {
    return monnaiesData;
  }

  static filtrerMonnaies(items) {
    return items.filter(it => it.type == 'monnaie');
  }

  static monnaiesManquantes(items) {
    const valeurs = Monnaie.filtrerMonnaies(items)
      .map(it => it.data.valeur_deniers)
    return duplicate(monnaiesData.filter(monnaie => !valeurs.find(v => v != monnaie.data.valeur_deniers)));
  }
}
