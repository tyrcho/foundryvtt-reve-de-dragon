des centaines de graines emport�es par le vent
des chemin�es de f�es
des cristaux de neige �tincelants au soleil
des feux follets dans la nuit
des fumeroles s'�chappant de fissures dans le sol
des gr�lons de la taille d'un oeuf de pigeon
des lichens � l'assaut d'une souche
des mirages sur l'horizon
des nuages accroch�es aux flancs d'une montagne
des nu�es dans le ciel nocturne
des plumes duveteuses accroch�es dans les foug�res
des roches sculpt�es par l'�rosion
des signes comme grav�s � m�me la pierre �voquant la langue des Dragons
des silhouettes impr�cises dans la brume
des sons de fl�tes provenant du sous bois
des traces de fossiles dans une roche
des uages noirs qui moutonnent juste avant la pluie
des veinures aux reflets m�talliques dans la roche
des voiles d'aurores bor�ales tombant dans le ciel nocturne
des �clairs z�brant le ciel � l'horizon
l'entrelacs des branches d'un arbre mill�naire
l'�coulement d'une chute l'eau
l'�cume sur les vagues sal�es
la brillance d'�toiles align�es
la coloration verte des flammes
la course hypnotique des balles d'un jongleur
la formation de givre sur une �tendue d'eau
la lueur du cr�puscule sur les cimes � l'horizon
la lune rouge sang
la ros�e dans une toile d'araign�e
la teinte rouge de la lune � travers les nuages
le faisceau d'ondes caus� par un insecte aquatique
le mouvement de grains de sable pouss�s par le vent
le mouvement de vagues battant le rivage
le mouvement r�gulier des pales d'un moulin � vent
le rythme de l'eau qui emporte les aubes d'un moulin
le soleil masqu� par un passage de nuages
le tableau abstrait de t�ches sur le sol
les cicatrices et boutons du visage d'un malade
les colorations violac�es et oranges du ciel matinal
les figures de la rouille sur un vieux casque
les filaments d'une fleur carnivore enla�ant un insecte
les rayons du soleil couchant
les reflets d'eau � travers les plantes aquatiques
les remous cascadant d�un torrent tels un liquide en �bullition dans une marmite de gigant
les restes d'un mur antique
les vagues du vent dans les herbes hautes
un arbre mort fendu par la foudre
un arc-en-ciel double
un arc-en-ciel tr�s lumineux
un cadavre d'animal inconnu
un dragon de nuages prenant son envol
un empilement de pierres
un enfant sautant dans les flaques d'eau
un fant�me de poussi�re �tincellant au soleil
un geyser projetant eau et vapeur � la face des Dragons
un manche d'outil poli par l'usure
un moustique gorg� de sang
un nid d'oiseau inconnu
un nuage d'insectes assombrissant le ciel
un panache de fum�e volcaniques s'�levant � l'horizon
un parterre de fleurs
un prairie brumeuse
un rideau de pluie �clair� d'un rai de lumi�re diffus
un tourbillon dans l'eau
un tourbillon de poussi�re
un vol d'oiseaux migrateurs align�s tra�ant des lettres dans le ciel
une braise attis�e par le vent
une coloration bleut�e de la lune
une concr�tion rocheuse �voquant une cascade fig�e
une fine couche de terre craquel�e par le soleil
une forme animale �voqu�e par les courbes d'une �corce
une gerbe d'�tincelles �chappant du feu
une mante religieuse d�vorrant son male
une mue de l�zard
une nu�e chaotique d'oiseaux tourbillonnant dans le vent
une ond�e illumin�e tombant tel un voile
une phosphorescence dans l'eau
une tornade dans le lointain
une �toile filante
une �trange disposition d'ossements sur le sol
